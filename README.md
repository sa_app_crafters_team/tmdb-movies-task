# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Android application consisting of two screens for displaying
  information TMDB movies in grid view and movie detail in other screen. The TMDB movies data should be obtained from
  the TMDB API at https://api.themoviedb.org/3/.

  The first screen have a list of now playing movies. I displayed all movies with pagination implemented also
  from the API. The app is an appropriate UI to indicate to the user that it is busy while
  it fetches results from the API. If the API request fails it displays an error message.

  When the user taps on a movie card the app switches to the second screen showing
  more detail about the movie and it's collection if available. The second screen shows labels and data values
  for each of the following movie attributes:
  • Title
  • release date
  • image
  • overview
  • collection (if available)

* Version 1.0
* Repository URL: https://shamoon192@bitbucket.org/shamoon192/tmdb-movies-task.git
* MVVM technical approach/design pattern is followed with data binding
* one Unit test is written named apiTest
* I have also used fragment navigation in code

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact