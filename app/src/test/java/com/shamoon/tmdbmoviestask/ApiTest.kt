package com.shamoon.tmdbmoviestask

import com.shamoon.tmdbmoviestask.api.Api
import com.shamoon.tmdbmoviestask.di.createRetrofitClient
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ApiTest {
	@Test
	fun apiNowPlayingTest() {
		assertTrue(
			createRetrofitClient().create(Api::class.java)
				.getNowPlayingMovies(api_key = "cb0be4a45d2350d26fdbe783cddf3d81")
				.execute().isSuccessful
		)
	}

	@Test
	fun apiMovieDetailTest() {
		assertTrue(
			createRetrofitClient().create(Api::class.java)
				.getMovieDetail(api_key = "cb0be4a45d2350d26fdbe783cddf3d81", movie_id = 460465)
				.execute().isSuccessful
		)
	}

	@Test
	fun apiCollectionTest() {
		assertTrue(
			createRetrofitClient().create(Api::class.java)
				.getCollection(api_key = "cb0be4a45d2350d26fdbe783cddf3d81", collection_id = 535313)
				.execute().isSuccessful
		)
	}
}