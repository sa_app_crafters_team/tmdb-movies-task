package com.shamoon.tmdbmoviestask.api

import com.shamoon.tmdbmoviestask.data.model.collection.CollectionResponse
import com.shamoon.tmdbmoviestask.data.model.movie.MovieDetailResponse
import com.shamoon.tmdbmoviestask.data.model.movies.MoviesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
	@GET("movie/now_playing")
	fun getNowPlayingMovies(
		@Query("api_key") api_key: String,
		@Query("language") language: String = "en-GB",
		@Query("page") page: Int = 1
	): Call<MoviesResponse>

	@GET("movie/{movie_id}")
	fun getMovieDetail(
		@Path("movie_id") movie_id: Int = 1,
		@Query("api_key") api_key: String,
		@Query("language") language: String = "en-GB"
	): Call<MovieDetailResponse>

	@GET("collection/{collection_id}")
	fun getCollection(
		@Path("collection_id") collection_id: Int = 1,
		@Query("api_key") api_key: String,
		@Query("language") language: String = "en-GB"
	): Call<CollectionResponse>
}