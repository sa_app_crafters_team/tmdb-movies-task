package com.shamoon.tmdbmoviestask.utilities

import android.content.Context
import android.net.ConnectivityManager

object Utils {
	fun isConnected(context: Context?): Boolean {
		val connectivityManager =
			context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		val netInfo = connectivityManager.activeNetworkInfo
		return netInfo != null && netInfo.isConnected
	}
}