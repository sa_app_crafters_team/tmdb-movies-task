package com.shamoon.tmdbmoviestask.utilities.exceptions

import java.lang.Exception

class NoConnectivityException(message: String?) : Exception(message)