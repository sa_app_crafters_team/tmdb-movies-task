package com.shamoon.tmdbmoviestask.utilities.listeners

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


abstract class EndlessRecyclerOnScrollListener : RecyclerView.OnScrollListener() {
	//    public static String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();

	/**
	 * The total number of items in the dataset after the last load
	 */
	private var mPreviousTotal: Int = 0

	/**
	 * True if we are still waiting for the last set of data to load.
	 */
	private var mLoading: Boolean = true

	override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
		super.onScrolled(recyclerView, dx, dy)
		val visibleItemCount = recyclerView.childCount;
		val totalItemCount = recyclerView.layoutManager?.itemCount;
		val firstVisibleItem =
			(recyclerView.layoutManager as GridLayoutManager?)?.findFirstVisibleItemPosition()
		if (mLoading) {
			if (totalItemCount ?: 0 > mPreviousTotal) {
				mLoading = false
				mPreviousTotal = totalItemCount ?: 0
			}
		}
		val visibleThreshold = 5;

		if (!mLoading && (totalItemCount ?: 0 - visibleItemCount)
			<= (firstVisibleItem ?: 0 + visibleThreshold)
		) {
			// End has been reached

			onLoadMore();

			mLoading = true;
		}
	}

	abstract fun onLoadMore()
}