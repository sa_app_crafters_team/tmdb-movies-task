package com.shamoon.tmdbmoviestask.utilities.bindings

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shamoon.tmdbmoviestask.R


object CustomViewBindings {
	@JvmStatic
	@BindingAdapter("setAdapter")
	fun bindAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
		recyclerView.setHasFixedSize(true)
		recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 3)
		recyclerView.adapter = adapter
	}

	@JvmStatic
	@BindingAdapter("setHorizontalAdapter")
	fun bindHorizontalAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
		recyclerView.setHasFixedSize(true)
		recyclerView.layoutManager =
			LinearLayoutManager(recyclerView.context).apply { orientation = RecyclerView.HORIZONTAL }
		recyclerView.adapter = adapter
	}

	@JvmStatic
	@BindingAdapter("imageUrl")
	fun loadImage(view: ImageView, url: String?) {
		Glide.with(view.context)
			.load("https://image.tmdb.org/t/p/w200/$url")
			.fitCenter()
			.placeholder(R.drawable.placeholder_no_image)
			.error(R.drawable.placeholder_no_image)
			.into(view)
	}
}