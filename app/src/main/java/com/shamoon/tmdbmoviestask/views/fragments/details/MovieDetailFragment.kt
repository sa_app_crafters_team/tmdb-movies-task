package com.shamoon.tmdbmoviestask.views.fragments.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.shamoon.tmdbmoviestask.R
import com.shamoon.tmdbmoviestask.data.model.collection.CollectionResponse
import com.shamoon.tmdbmoviestask.data.model.movie.MovieDetailResponse
import com.shamoon.tmdbmoviestask.databinding.MovieDetailFragmentBinding
import com.shamoon.tmdbmoviestask.utilities.Utils
import com.shamoon.tmdbmoviestask.viewmodels.MovieDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailFragment : Fragment() {

	private val viewModel: MovieDetailViewModel by viewModel()
	private lateinit var binding: MovieDetailFragmentBinding
	val args: MovieDetailFragmentArgs by navArgs()

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		binding =
			DataBindingUtil.inflate(inflater, R.layout.movie_detail_fragment, container, false)
		return binding.root
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		setupBinding()
	}

	private fun setupBinding() {
		binding.viewModel = viewModel

		if (Utils.isConnected(context)){
			viewModel.getMovieDetail(args.id, getString(R.string.api_key))
			viewModel.showEmpty.set(View.GONE)
			viewModel.showContent.set(View.VISIBLE)
		}else{
			viewModel.showEmpty.set(View.VISIBLE)
			viewModel.showContent.set(View.GONE)
		}


		val observerCollection =
			Observer<com.shamoon.tmdbmoviestask.api.Result<CollectionResponse>> {
				// Update the UI, for recycler view list item.
				when (it) {
					is com.shamoon.tmdbmoviestask.api.Result.Success -> {
						viewModel.loading.set(View.GONE)
						viewModel.setCollection(it.data)
					}
					is com.shamoon.tmdbmoviestask.api.Result.Loading -> {
						viewModel.loading.set(View.VISIBLE)
					}
					is com.shamoon.tmdbmoviestask.api.Result.Error -> {
						viewModel.loading.set(View.GONE)
						viewModel.showEmpty.set(View.VISIBLE)
					}
				}
			}

		viewModel.eventGotCollection.observe(this) {
			viewModel.observeCollection().observe(viewLifecycleOwner, observerCollection)
		}
		viewModel.eventMovieClicked.observe(this) {
			try {
				viewModel.getMovieDetail(it, getString(R.string.api_key))
			} catch (e: Exception) {
				e.printStackTrace()
			}
		}

		val observer =
			Observer<com.shamoon.tmdbmoviestask.api.Result<MovieDetailResponse>> {
				// Update the UI, for recycler view list item.
				when (it) {
					is com.shamoon.tmdbmoviestask.api.Result.Success -> {
						viewModel.loading.set(View.GONE)
						viewModel.setDetail(it.data, getString(R.string.api_key))
						(activity as AppCompatActivity).supportActionBar?.title = it.data.title
					}
					is com.shamoon.tmdbmoviestask.api.Result.Loading -> {
						viewModel.loading.set(View.VISIBLE)
					}
					is com.shamoon.tmdbmoviestask.api.Result.Error -> {
						viewModel.loading.set(View.GONE)
						viewModel.showEmpty.set(View.VISIBLE)
					}
				}
			}

		viewModel.observeData().observe(viewLifecycleOwner, observer)
	}

}