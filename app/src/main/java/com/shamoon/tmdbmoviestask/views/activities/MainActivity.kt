package com.shamoon.tmdbmoviestask.views.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shamoon.tmdbmoviestask.R

class MainActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
	}
}