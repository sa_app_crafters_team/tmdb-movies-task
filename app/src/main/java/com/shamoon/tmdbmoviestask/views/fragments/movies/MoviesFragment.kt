package com.shamoon.tmdbmoviestask.views.fragments.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnScrollChangedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.shamoon.tmdbmoviestask.R
import com.shamoon.tmdbmoviestask.data.model.movies.MoviesResponse
import com.shamoon.tmdbmoviestask.databinding.MoviesFragmentBinding
import com.shamoon.tmdbmoviestask.utilities.listeners.EndlessRecyclerOnScrollListener
import com.shamoon.tmdbmoviestask.viewmodels.MoviesViewModel
import com.shamoon.tmdbmoviestask.views.activities.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class MoviesFragment : Fragment() {
	val viewModel: MoviesViewModel by viewModel()
	private lateinit var binding: MoviesFragmentBinding


	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		binding = DataBindingUtil.inflate(inflater, R.layout.movies_fragment, container, false)
		if(activity is MainActivity){
			(activity as AppCompatActivity).supportActionBar?.title = getString(R.string.movies)
		}
		return binding.root
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		setupBindings()
	}

	override fun onDestroyView() {
		super.onDestroyView()
		viewModel.resetCurrentPage()
	}

	private fun setupBindings() {
		binding.viewModel = viewModel

		viewModel.eventMovieClicked.observe(this) {
			try {
				val bundle = Bundle()
				bundle.putInt("id", it)
				this.findNavController()
					.navigate(R.id.action_moviesFragment_to_movieDetailFragment, bundle)
			} catch (e: Exception) {
				e.printStackTrace()
			}
		}
		viewModel.setApiKey(getString(R.string.api_key))

		viewModel.getNowPlayingMovies()

		val observer =
			Observer<com.shamoon.tmdbmoviestask.api.Result<MoviesResponse>> {
				// Update the UI, for recycler view list item.
				when (it) {
					is com.shamoon.tmdbmoviestask.api.Result.Success -> {
						viewModel.loading.set(View.GONE)
						viewModel.setMoviesInAdapter(it.data.results)
					}
					is com.shamoon.tmdbmoviestask.api.Result.Loading -> {
						viewModel.loading.set(View.VISIBLE)
					}
					is com.shamoon.tmdbmoviestask.api.Result.Error -> {
						viewModel.loading.set(View.GONE)
						viewModel.showEmpty.set(View.VISIBLE)
					}
				}
			}
		viewModel.observeData().observe(viewLifecycleOwner, observer)

		binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
			override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
				super.onScrollStateChanged(recyclerView, newState)
				if (!recyclerView.canScrollVertically(1)) {
					viewModel.loadMore()
				}
			}
		})
	}

}