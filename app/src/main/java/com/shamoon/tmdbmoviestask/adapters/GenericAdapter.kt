package com.shamoon.tmdbmoviestask.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.shamoon.tmdbmoviestask.BR
import com.shamoon.tmdbmoviestask.viewmodels.ListItemViewModel

class GenericAdapter<T : ListItemViewModel>(@LayoutRes val layoutId: Int) :
	RecyclerView.Adapter<GenericAdapter.GenericViewHolder<T>>() {

	private val items = mutableListOf<T>()
	private var inflater: LayoutInflater? = null
	private var onListItemViewClickListener: OnListItemViewClickListener? = null

	fun addItems(items: List<T>) {
		//this.items.clear()
		val prevCount = this.items.size
		this.items.addAll(items)
		notifyItemRangeInserted(prevCount, this.items.count())
	}

	fun setOnListItemViewClickListener(onListItemViewClickListener: OnListItemViewClickListener?) {
		this.onListItemViewClickListener = onListItemViewClickListener
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<T> {
		val layoutInflater = inflater ?: LayoutInflater.from(parent.context)
		val binding =
			DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, layoutId, parent, false)
		return GenericViewHolder(binding)
	}

	override fun getItemCount(): Int = items.size

	override fun onBindViewHolder(holder: GenericViewHolder<T>, position: Int) {
		val itemViewModel = items[position]
		itemViewModel.adapterPosition = position
		onListItemViewClickListener?.let { itemViewModel.onListItemViewClickListener = it }
		holder.bind(itemViewModel)
	}


	class GenericViewHolder<T : ListItemViewModel>(private val binding: ViewDataBinding) :
		RecyclerView.ViewHolder(binding.root) {

		fun bind(itemViewModel: T) {
			binding.setVariable(BR.listItemViewModel, itemViewModel)
			binding.executePendingBindings()
		}

	}

	interface OnListItemViewClickListener {
		fun onListItemClick(view: View, id: Int)
	}
}