package com.shamoon.tmdbmoviestask.data.model.movies

data class Dates(
    val maximum: String,
    val minimum: String
)