package com.shamoon.tmdbmoviestask.data.model.collection

data class CollectionResponse(
    val backdrop_path: String,
    val id: Int,
    val name: String,
    val overview: String,
    val parts: ArrayList<Part>,
    val poster_path: String
)