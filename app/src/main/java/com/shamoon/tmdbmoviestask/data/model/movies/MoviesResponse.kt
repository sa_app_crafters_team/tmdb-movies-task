package com.shamoon.tmdbmoviestask.data.model.movies

data class MoviesResponse(
    val dates: Dates,
    val page: Int,
    val results: ArrayList<Result>,
    val total_pages: Int,
    val total_results: Int
)