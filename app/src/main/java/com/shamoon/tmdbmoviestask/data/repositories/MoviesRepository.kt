package com.shamoon.tmdbmoviestask.data.repositories
import androidx.lifecycle.MutableLiveData
import com.shamoon.tmdbmoviestask.api.Api
import com.shamoon.tmdbmoviestask.api.Result
import com.shamoon.tmdbmoviestask.data.model.movies.MoviesResponse

class MoviesRepository(private val api: Api) {
	fun getNowPlayingMovies(
		liveData: MutableLiveData<Result<MoviesResponse>>,
		page: Int,
		api_key: String
	) {
		liveData.value = Result.Loading(true)
		api.getNowPlayingMovies(page = page, api_key = api_key)
			.enqueue(GenericCallback(liveData))
	}
}