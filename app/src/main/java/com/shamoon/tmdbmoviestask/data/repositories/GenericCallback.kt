package com.shamoon.tmdbmoviestask.data.repositories

import androidx.lifecycle.MutableLiveData
import com.shamoon.tmdbmoviestask.api.Result
import com.shamoon.tmdbmoviestask.utilities.extensions.isValid
import org.koin.android.logger.AndroidLogger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenericCallback<T : Any>(private val liveData: MutableLiveData<Result<T>>) : Callback<T> {
	override fun onResponse(call: Call<T>, response: Response<T>) {
		if (response.isSuccessful) {
			liveData.postValue(Result.Success(response.body()!!))
		} else {
			if (response.message().isValid()) {
				liveData.value = Result.Error.RecoverableError(Exception(response.message()))
			} else {
				liveData.value = Result.Error.NonRecoverableError(Exception("Un-traceable"))
			}
		}
	}

	override fun onFailure(call: Call<T>, t: Throwable) {
		AndroidLogger().debug(t.message ?: "onFailure: getNowPlayingMovies")
		if (t.message.isValid()) {
			liveData.value =
				Result.Error.RecoverableError(Exception(t.message.toString()))
		} else {
			liveData.value = Result.Error.NonRecoverableError(Exception("Un-traceable"))
		}
	}

}