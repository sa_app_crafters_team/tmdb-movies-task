package com.shamoon.tmdbmoviestask.data.repositories

import androidx.lifecycle.MutableLiveData
import com.shamoon.tmdbmoviestask.api.Api
import com.shamoon.tmdbmoviestask.api.Result
import com.shamoon.tmdbmoviestask.data.model.movie.MovieDetailResponse

class MovieDetailRepository(private val api: Api) {
	fun getMovieDetail(
		liveData: MutableLiveData<Result<MovieDetailResponse>>,
		movieId: Int,
		api_key: String
	) {
		liveData.value = Result.Loading(true)
		api.getMovieDetail(movie_id = movieId, api_key = api_key).enqueue(GenericCallback(liveData))
	}
}