package com.shamoon.tmdbmoviestask.data.model.movie

data class Genre(
    val id: Int,
    val name: String
)