package com.shamoon.tmdbmoviestask.data.repositories

import androidx.lifecycle.MutableLiveData
import com.shamoon.tmdbmoviestask.api.Api
import com.shamoon.tmdbmoviestask.api.Result
import com.shamoon.tmdbmoviestask.data.model.collection.CollectionResponse

class CollectionRepository(private val api: Api) {
	fun getCollection(
		liveData: MutableLiveData<Result<CollectionResponse>>,
		collectionId: Int,
		api_key: String
	) {
		liveData.value = Result.Loading(true)
		api.getCollection(collection_id = collectionId, api_key = api_key).enqueue(GenericCallback(liveData))
	}
}