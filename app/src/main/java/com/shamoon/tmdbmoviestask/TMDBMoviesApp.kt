package com.shamoon.tmdbmoviestask

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.shamoon.tmdbmoviestask.di.networkModule
import com.shamoon.tmdbmoviestask.di.viewmodelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TMDBMoviesApp : MultiDexApplication() {
	override fun attachBaseContext(base: Context?) {
		super.attachBaseContext(base)
		MultiDex.install(this)
	}

	override fun onCreate() {
		super.onCreate()
		setupDependencyInjection()
	}

	private fun setupDependencyInjection() {
		startKoin {
			androidContext(this@TMDBMoviesApp)
			androidLogger()
			modules(listOf(viewmodelModule, networkModule))
		}
	}
}