package com.shamoon.tmdbmoviestask.viewmodels

import com.shamoon.tmdbmoviestask.adapters.GenericAdapter

abstract class ListItemViewModel{
	var adapterPosition: Int = -1
	var onListItemViewClickListener: GenericAdapter.OnListItemViewClickListener? = null
}