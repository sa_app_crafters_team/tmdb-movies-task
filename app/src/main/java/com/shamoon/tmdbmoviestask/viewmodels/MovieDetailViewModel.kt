package com.shamoon.tmdbmoviestask.viewmodels

import android.view.View
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shamoon.tmdbmoviestask.R
import com.shamoon.tmdbmoviestask.adapters.GenericAdapter
import com.shamoon.tmdbmoviestask.data.model.collection.CollectionResponse
import com.shamoon.tmdbmoviestask.data.model.collection.Part
import com.shamoon.tmdbmoviestask.data.model.movie.MovieDetailResponse
import com.shamoon.tmdbmoviestask.data.repositories.CollectionRepository
import com.shamoon.tmdbmoviestask.data.repositories.MovieDetailRepository
import com.shamoon.tmdbmoviestask.utilities.events.SingleLiveEvent

class MovieDetailViewModel(
	private val repository: MovieDetailRepository,
	private val collectionRepository: CollectionRepository
) : ViewModel(), GenericAdapter.OnListItemViewClickListener {
	private val adapter: GenericAdapter<Part> =
		GenericAdapter(R.layout.item_collection_list)
	private val movieDetailRepoLiveData =
		MutableLiveData<com.shamoon.tmdbmoviestask.api.Result<MovieDetailResponse>>()
	private val collectionRepoLiveData =
		MutableLiveData<com.shamoon.tmdbmoviestask.api.Result<CollectionResponse>>()
	var loading = ObservableInt(View.VISIBLE)
	var showEmpty = ObservableInt(View.GONE)
	var showContent = ObservableInt(View.GONE)
	var detail: ObservableField<MovieDetailResponse> = ObservableField()
	var collection: ObservableField<CollectionResponse> = ObservableField()
	val eventGotCollection = SingleLiveEvent<Void>()
	val eventMovieClicked = SingleLiveEvent<Int>()

	fun setDetail(movieDetail: MovieDetailResponse, apiKey: String) {
		detail.set(movieDetail)
		if (movieDetail.belongs_to_collection != null) {
			collectionRepository.getCollection(
				liveData = collectionRepoLiveData,
				collectionId = movieDetail.belongs_to_collection.id,
				api_key = apiKey
			)
			eventGotCollection.call()
		}
	}

	fun getCollectionAdapter(): GenericAdapter<Part> {
		return adapter
	}

	fun setCollection(collection: CollectionResponse) {
		this.collection.set(collection)
		adapter.addItems(collection.parts)
		adapter.setOnListItemViewClickListener(this)
	}

	fun observeCollection(): MutableLiveData<com.shamoon.tmdbmoviestask.api.Result<CollectionResponse>> {
		return collectionRepoLiveData
	}

	fun observeData(): MutableLiveData<com.shamoon.tmdbmoviestask.api.Result<MovieDetailResponse>> {
		return movieDetailRepoLiveData
	}

	fun getMovieDetail(movieId: Int, apiKey: String) {
		repository.getMovieDetail(
			liveData = movieDetailRepoLiveData,
			movieId = movieId,
			api_key = apiKey
		)
	}

	override fun onListItemClick(view: View, id: Int) {
		eventMovieClicked.call(id)
	}
}