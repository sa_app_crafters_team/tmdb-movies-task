package com.shamoon.tmdbmoviestask.viewmodels

import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shamoon.tmdbmoviestask.R
import com.shamoon.tmdbmoviestask.adapters.GenericAdapter
import com.shamoon.tmdbmoviestask.data.model.movies.MoviesResponse
import com.shamoon.tmdbmoviestask.data.model.movies.Result
import com.shamoon.tmdbmoviestask.data.repositories.MoviesRepository
import com.shamoon.tmdbmoviestask.utilities.events.SingleLiveEvent

class MoviesViewModel(private val repository: MoviesRepository) : ViewModel(), GenericAdapter.OnListItemViewClickListener {

	private val moviesRepoLiveData = MutableLiveData<com.shamoon.tmdbmoviestask.api.Result<MoviesResponse>>()
	private val adapter : GenericAdapter<Result> =
		GenericAdapter(R.layout.item_movies_list)
	var loading = ObservableInt(View.VISIBLE)
	var showEmpty = ObservableInt(View.GONE)
	private var currentPage = 1
	private lateinit var apiKey: String

	fun setApiKey(apiKey: String){
		this.apiKey = apiKey
	}

	val eventMovieClicked = SingleLiveEvent<Int>()
	init {
		adapter.setOnListItemViewClickListener(this)
	}

	fun getMoviesAdapter(): GenericAdapter<Result> {
		return adapter
	}
	fun setMoviesInAdapter(movies: ArrayList<Result>) {
		adapter.addItems(movies)
	}
	fun observeData() : MutableLiveData<com.shamoon.tmdbmoviestask.api.Result<MoviesResponse>> {
		return moviesRepoLiveData
	}

	fun getNowPlayingMovies(){
		repository.getNowPlayingMovies(liveData = moviesRepoLiveData, page = currentPage, api_key = apiKey)
	}

	override fun onListItemClick(view: View, id: Int) {
		eventMovieClicked.call(id)
	}

	fun resetCurrentPage() {
		currentPage = 1
	}

	fun loadMore(){
		currentPage += 1
		getNowPlayingMovies()
	}
}