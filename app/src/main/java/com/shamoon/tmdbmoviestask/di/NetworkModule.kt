package com.shamoon.tmdbmoviestask.di

import com.shamoon.tmdbmoviestask.BuildConfig
import com.shamoon.tmdbmoviestask.api.Api
import com.shamoon.tmdbmoviestask.data.repositories.CollectionRepository
import com.shamoon.tmdbmoviestask.data.repositories.MovieDetailRepository
import com.shamoon.tmdbmoviestask.data.repositories.MoviesRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


fun createRetrofitClient() =
	retrofitClient(baseUrl, okHttpClient())


val networkModule = module {
	single { createRetrofitClient().create(Api::class.java) }
	single { MoviesRepository(get()) }
	single { MovieDetailRepository(get()) }
	single { CollectionRepository(get()) }
}


private val baseUrl = "https://api.themoviedb.org/3/"
fun okHttpClient() =
	OkHttpClient.Builder().run {
		addInterceptor(HttpLoggingInterceptor().apply {
			if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
			readTimeout(60L, TimeUnit.SECONDS)
			connectTimeout(60L, TimeUnit.SECONDS)
			writeTimeout(60L, TimeUnit.SECONDS)
		})
		build()
	}

private fun retrofitClient(baseUrl: String, httpClient: OkHttpClient): Retrofit =
	Retrofit.Builder().run {
		baseUrl(baseUrl)
		client(httpClient)
		addConverterFactory(GsonConverterFactory.create())
		build()
	}