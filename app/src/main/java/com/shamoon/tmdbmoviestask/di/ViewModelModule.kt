package com.shamoon.tmdbmoviestask.di

import com.shamoon.tmdbmoviestask.viewmodels.MovieDetailViewModel
import com.shamoon.tmdbmoviestask.viewmodels.MoviesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewmodelModule = module {
	viewModel { MoviesViewModel(get()) }
	viewModel { MovieDetailViewModel(get(), get()) }
}